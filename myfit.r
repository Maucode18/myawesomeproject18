#!/usr/bin/env Rscript

setwd("~/CCII/myawesomeproject18/")

myfit <- function(myarray) {
  
  h <- hist(myarray, plot = FALSE)
  y <- log(h$density)
  x <- h$mids
  
  n <- length(x)
  sum_x <- sum(x)
  sum_y <- sum(y)
  sum_xy <- sum(x*y)
  sum_x2 <- sum(x*x)
  
  m <- (n*sum_xy - sum_y*sum_x)/(n*sum_x2 - (sum_x)^2)
  b <- (sum_y - m*sum_x)/n
  
  myarray <- c(m,b)
  return (myarray)

}

estimate_variations <- function (df,n) {
  
  marray <- c()
  barray <- c()
  
  for (i in seq (1,n)) {
    
    sn <- sample (df$decaytime, length(df$decaytime), replace = T)
    pn <- myfit(sn)
    
    marray <- c(marray, pn[1])
    barray <- c(barray, pn[2])
  }
  
outdf <- data.frame(marray, barray)
colnames(outdf) <- c("m", "b")
outdf <- na.omit(outdf)

return (outdf)
}

df <- read.table("data.csv", header= T)
dfparameters <- estimate_variations (df, 1000)
hm <- hist(dfparameters$m)
hb <- hist(dfparameters$b)

sigmam <- sd(dfparameters$m)
sigmab <- sd(dfparameters$b)

print (sigmam)
print (sigmab)

h <- hist(df$decaytime)
fitresults <- myfit(df$decaytime)

x <- h$mids
y <- length(df$decaytime)*200*exp(fitresults[1]*h$mids + fitresults[2])
lines(x,y)

m_high <- fitresults[1] + sigmam
m_low <- fitresults[1] - sigmam

b_high <- fitresults[2] + sigmab
b_low <- fitresults[2] - sigmab

x_high <- h$mids
y_high <- length(df$decaytime)*200*exp(m_high*h$mids + b_high)
lines(x_high,y_high, col = rgb(1,0,0,1))

x_low <- h$mids
y_low <- length(df$decaytime)*200*exp(m_low*h$mids + b_low)
lines(x_low,y_low, col = rgb(0,0,1,1))




