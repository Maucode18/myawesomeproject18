#! usr/bin/env Rscript

setwd("~/Desktop/CCII/myawesomeproject18/Exam")
df <- read.table("exam_data.csv", sep = ',', header = T)
source('source.r')

#histogram mass 

h <- hist(df$mass, breaks = 120, main = "Histogram mass", xlab = "Mass (MeV/c^2)", ylab = "Frequency", col = "orange", ylim = c(0,600))
h_m <- c(df$mass)

#parameters
#(central, s_l, s_r)

m_p1 <-  c(6.379841, 6.379841 - 0.017013, 6.379841 + 0.021266)
m_p2 <- c(3.278644, 3.278644 - 0.004372, 3.278644 + 0.006557)
lamb_p1 <- c(426.9271131, 426.9271131 - 3.0924311, 426.9271131 + 3.946286)
lamb_p2 <- c(473.07703, 473.07703 - 8.515387, 473.07703 + 8.830771)
c <- c(0.080442,0.080442 - 0.003698, 0.080442 + 0.003483)
lamb_a <- c(97.296332, 97.296332 - 2.789162, 97.296332 + 2.918890)
lamb_b <- c(695.5207991, 695.5207991 - 1.1283331, 695.5207991 + 2.055694)
s1 <- c(0.438460, 0.438460 - 0.012277, 0.438460 + 0.013154)
s2 <- c(0.225270, 0.225270 - 0.003755, 0.225270 + 0.004055)
alpha <- c(0.146767, 0.146767 - 0.004107, 0.146767 + 0.004205)
beta <- c(0.284414, 0.284414 - 0.004171, 0.284414  + 0.004551)
gama <- c(0.248635, 0.248635 - 0.004973,0.248635 + 0.005138)
delt <- c(0.298930, 0.298930 - 0.000000, 0.298930 + 0.000214)

N <- (max(h_m)-min(h_m)) - 0.5*c[1]*(max(h_m)*max(h_m)-min(h_m)*min(h_m))
x_names <- c("x.c","x.l","x.r")
y_names <-  c("y.c","y.l","y.r")



y.c <-(alpha[1]*(1/( s1[1] * sqrt(2*pi)))*(exp(-(h$mids-m_p1[1])*(h$mids-m_p1[1])/(2*s1[1]*s1[1]))) + 
  beta[1]*(1/( s2[1] * sqrt(2*pi)))*(exp(-(h$mids-m_p2[1])*(h$mids-m_p2[1])/(2*s2[1]*s2[1]))) + 
  gama[1]*1/(max(h_m)-min(h_m)) + 
  delt[1]*((1- c[1]*h$mids)/N))*((max(h_m)-min(h_m))/120)*length(h_m)

y.l <- (alpha[2]*(1/( s1[2] * sqrt(2*pi)))*(exp(-(h$mids-m_p1[2])*(h$mids-m_p1[2])/(2*s1[2]*s1[2]))) + 
        beta[2]*(1/( s2[2] * sqrt(2*pi)))*(exp(-(h$mids-m_p2[2])*(h$mids-m_p2[2])/(2*s2[2]*s2[2]))) + 
        gama[2]*1/(max(h_m)-min(h_m)) + 
        delt[2]*((1- c[2]*h$mids)/N))*((max(h_m)-min(h_m))/120)*length(h_m)

y.r <- (alpha[3]*(1/( s1[3] * sqrt(2*pi)))*(exp(-(h$mids-m_p1[3])*(h$mids-m_p1[3])/(2*s1[3]*s1[3]))) + 
          beta[3]*(1/( s2[3] * sqrt(2*pi)))*(exp(-(h$mids-m_p2[3])*(h$mids-m_p2[3])/(2*s2[3]*s2[3]))) + 
          gama[3]*1/(max(h_m)-min(h_m)) + 
          delt[3]*((1- c[3]*h$mids)/N))*((max(h_m)-min(h_m))/120)*length(h_m)

x.c <- h$mids

#lines(x.c, y.l, col = "brown", lty = 3, lwd = 2)
#lines (x.c, y.r, col = "blue", lwd = 2, lty = 5)
#lines(x.c, y.c, col = "red", lwd = 2)

#  legend(5,500, c("Sigma right", "Sigma left", "Central point"),
#         lty=c(3,5,1),lwd=c(2,2,2),col=c("brown", "blue", "red"))

h1 <- hist(df$decaylength, breaks = 120, main = "Histogram length decay", xlab = "Length (mm)", ylab = "Frequency", col = "light blue")
h_l <- c(df$decaylength)

#central
  
l.c <-  (alpha[1]*(1/lamb_p1[1])*(exp((-1)*h1$mids/lamb_p1[1])) +
beta[1]*(1/lamb_p2[1])*(exp((-1)*h1$mids/lamb_p2[1])) +
gama[1]*(1/lamb_a[1])*(exp((-1)*h1$mids/lamb_a[1])) +
delt[1]*(1/lamb_b[1])*(exp((-1)*h1$mids/lamb_b[1])))*length(h_l)*(max(h_l)-min(h_l))/120

l.l <- (alpha[2]*(1/lamb_p1[2])*(exp((-1)*h1$mids/lamb_p1[2])) +
          beta[2]*(1/lamb_p2[2])*(exp((-1)*h1$mids/lamb_p2[2])) +
          gama[2]*(1/lamb_a[2])*(exp((-1)*h1$mids/lamb_a[2])) +
          delt[2]*(1/lamb_b[2])*(exp((-1)*h1$mids/lamb_b[2])))*length(h_l)*(max(h_l)-min(h_l))/120

l.r <- (alpha[3]*(1/lamb_p1[3])*(exp((-1)*h1$mids/lamb_p1[3])) +
          beta[3]*(1/lamb_p2[3])*(exp((-1)*h1$mids/lamb_p2[3])) +
          gama[3]*(1/lamb_a[3])*(exp((-1)*h1$mids/lamb_a[3])) +
          delt[3]*(1/lamb_b[3])*(exp((-1)*h1$mids/lamb_b[3])))*length(h_l)*(max(h_l)-min(h_l))/120

ll.c <- h1$mids

#lines(ll.c, l.r, col = "blue", lwd = 2)
#lines(ll.c, l.l, col = "green", lwd = 2)
#lines(ll.c, l.c, col= "red", lwd = 2)


#legend(3000,2000, c("Sigma right", "Sigma left", "Central point"),
#       lty=c(1,1,1),lwd=c(2,2,2),col=c("blue", "green", "red"))
