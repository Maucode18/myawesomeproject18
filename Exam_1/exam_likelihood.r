#! usr/bin/env Rscript

library(MASS)

setwd("~/Desktop/CCII/myawesomeproject18/Exam")
df <- read.table("exam_data.csv", sep = ',', header = T)
source('source.r')

#Here starts the Newton-Raphson method
#Requirements

der_f <- c(1:12)
jac_f <- matrix(1:144, nrow = 12)
dif <- -100000
counter <- 0
eps <- 1e-7
c <- 0.1

# Performing test_exam
#s1 = 0.5, s2 = 0.2, alpha = 0.2, beta = 0.3, gama = 0.2

x_0 <- c(6.36, 3.27, 460, 470, c , 100, 700, 0.5, 0.2, 0.2, 0.3, 0.2)
print(log_lh(df, x_0))

#Generation of jacobian and best parameters x_n1

#We assume lamb_p1 != lamb_p2, but their lifetimes are aprroximattely 460, that is maybe
# lifetime_p1 = 460, ligetime_p2 = 470

while (abs(dif)>eps){
  for (i in 1:12){
    der_f[i] <- dv(log_lh, df, x_0, i) 
    for (j in 1:12){
      jac_f[i, j] <- dv2(log_lh, df , x_0 , i , j)  
    }
  }

  Jf <- ginv(jac_f) %*% der_f
  x_n <- x_0 - Jf
  x_n1 <- x_n - x_0
  dif <- sqrt(sum(x_n1*x_n1))
  x_0 <- x_n
  counter <- counter+1
  
  print(counter)
  print(x_0)
  print(dif)
  if(counter==100){break}
}

