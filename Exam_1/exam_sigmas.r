#! usr/bin/env Rscript

library(MASS)

setwd("~/Desktop/CCII/myawesomeproject18/Exam")
df <- read.table("exam_data.csv", sep = ',', header = T)
source('source.r')

#x_0 obtained by Maximum likelihood

x_0 = c(6.37984134, 3.27864430, 426.92711252, 473.07703819, 0.08038828, 97.29633228, 695.52079929, 0.43846029, 0.22527021, 0.14666943, 0.28441381, 0.24863475, 1 - 0.14666943 - 0.28441381 - 0.24863475)
s = c("Mass particle 1 ", "Mass particle 2 ", "Lambda particle 1 ", "Lambda particle 2", 
"Constant c", "Lambda noise a", "Lambda noise b", "Sigma particle 1", "Sigma particle 2", "Alpha" ,
"Beta", "Gamma")

#Determination of left sigma, right sigma for every parameter

x_part <- c()
for (i in 1:13){ 
  x_results <- compute_errors(df, x_0, i, log_lh, s)
  
  if ((i==10)|(i==11)|(i==12)|(i==13)){
    x_part <- c(x_part, x_results)
  }
}

#Number or particles

part_seq <- seq(from = 1, to = 12, by = 3)
h <- c(df$mass)
sp <- c("Number particles 1:","","","Numbers particles 2:","","","Number events noise a:","","","Number events noise b:","","")
print (length(h))


for (i in part_seq) {
  
  particles.c <- length(h)*x_part[i]
  particles.l <- length(h)*x_part[i+1]
  particles.r <- length(h)*x_part[i+2]
  
  print(sp[i])
  print (particles.c)
  print("Sigma left: " )
  print(particles.l)
  print("Sigma right: ")
  print(particles.r)
  
}

#p-value-mass

m_p1 <- x_0[1]  
m_p2 <- x_0[2]
lamb_p1 <- x_0[3]
lamb_p2 <- x_0[4]
c <- x_0[5]
lamb_a <- x_0[6]
lamb_b <- x_0[7]
s1 <- x_0[8]
s2 <- x_0[9]
alpha <- x_0[10]
beta <- x_0[11]
gama <- x_0[12]

#Distributions mass, p-value

nbins <- 50
step_m <- (max(df$mass)- min(df$mass))/nbins

breiks_m <- seq(min(df$mass), max(df$mass), step_m)
hdatos_m <- hist(df$mass, breaks = breiks_m)
ni_m <- hdatos_m$counts

model_mp1 <- (1/( s1 * sqrt(2*pi)))*(exp(-(h-m_p1)*(h-m_p1)/(2*s1*s1)))
model_mp2 <- (1/( s2 * sqrt(2*pi)))*(exp(-(h-m_p2)*(h-m_p2)/(2*s2*s2)))

N <- (max(h)-min(h)) - 0.5*c*(max(h)*max(h)-min(h)*min(h))

model_noise_a <- 1/(max(h)-min(h))
model_noise_b <- (1- c*h)/N

model_total_m <- alpha*model_mp1 + beta*model_mp2 + gama*model_noise_a + (1-alpha-beta-gama)*model_noise_b

#xi² 

gnu_i_m = model_total_m * step_m * length(df$mass)
xsq_m <- sum((ni_m - gnu_i_m)*(ni_m - gnu_i_m)/(gnu_i_m))

goodfit_m <- xsq_m/(nbins-1)

pvalue_m <- 1 - pchisq(xsq_m, nbins - 1)
#print(gnu_i)
#print(goodfit_m)
print("P-value mass: ")
print(pvalue_m)

#P-value, decay length

nbins <- 50
step_l <- (max(df$decaylength)- min(df$decaylength))/nbins

breiks_l <- seq(min(df$decaylength), max(df$decaylength), step_l)
hdatos_l <- hist(df$decaylength, breaks = breiks_l)
ni_l <- hdatos_l$counts

l <- c(df$decaylength)

model_lamb_p1 <- (1/lamb_p1)*(exp((-1)*l/lamb_p1))
model_lamb_p2 <- (1/lamb_p2)*(exp((-1)*l/lamb_p2))


model_lamb_a <- (1/lamb_a)*(exp((-1)*l/lamb_a))
model_lamb_b <- (1/lamb_b)*(exp((-1)*l/lamb_b)) 

model_total_lamb <- alpha*model_lamb_p1 + beta*model_lamb_p2 + gama*model_lamb_a + (1-alpha-beta-gama)*model_lamb_b

#xi² 

gnu_i_l = model_total_lamb * step_l * length(df$decaylength)
xsq_l <- sum((ni_l - gnu_i_l)*(ni_l - gnu_i_l)/(gnu_i_l))

goodfit_l <- xsq_l/(nbins-1)

pvalue_l <- 1 - pchisq(xsq_l, nbins - 1)
#print(gnu_i)
print(goodfit)
print("P-value length: ")
print(pvalue_l)