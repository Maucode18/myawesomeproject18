#! usr/bin/env Rscript

library(MASS)

setwd("~/Desktop/CCII/myawesomeproject18/Exam")
df <- read.table("exam_data.csv", sep = ',', header = T)

log_lh <- function(data, par) {
  
  m_p1 <- par[1]  
  m_p2 <- par[2]
  lamb_p1 <- par[3]
  lamb_p2 <- par[4]
  c <- par[5]
  lamb_a <- par[6]
  lamb_b <- par[7]
  s1 <- par[8]
  s2 <- par[9]
  alpha <- par[10]
  beta <- par[11]
  gama <- par[12]
  
  l <- data[ , "decaylength" ]
  m <- data[ , "mass" ]
  
  #The particles' p1, p2 masses follow a gaussian distribution
  
  pdf_mp1 <- (1/( s1 * sqrt(2*pi)))*(exp(-(m-m_p1)*(m-m_p1)/(2*s1*s1)))
  pdf_mp2 <- (1/( s2 * sqrt(2*pi)))*(exp(-(m-m_p2)*(m-m_p2)/(2*s2*s2)))
  
  #The decaytime is the inverse of the decay rate. Following a exponential decay, the pdf 
  # of the length distribution can be expressed in terms of the lifetime. We suppose 
  # different lambdas, that is, different lifetimes.
  
  pdf_lamb_p1 <- (1/lamb_p1)*(exp((-1)*l/lamb_p1))
  pdf_lamb_p2 <- (1/lamb_p2)*(exp((-1)*l/lamb_p2))
  
  #The length of the background noise follows a exponential decay
  
  pdf_lamb_a <- (1/lamb_a)*(exp((-1)*l/lamb_a))
  pdf_lamb_b <- (1/lamb_b)*(exp((-1)*l/lamb_b))                      
  
  #The background noise mass follows the next distributions
  
  N <- (max(m)-min(m)) - 0.5*c*(max(m)*max(m)-min(m)*min(m))
  
  pdf_noise_a <- 1/(max(m)-min(m))
  pdf_noise_b <- (1- c*m)/N
  
  # We compute the total pdf
  
  pdf_tot <- alpha*pdf_mp1*pdf_lamb_p1 + beta*pdf_mp2*pdf_lamb_p2 + gama*pdf_lamb_a*pdf_noise_a + (1-alpha-beta-gama)*pdf_lamb_b*pdf_noise_b 
  log_like <- sum(log(pdf_tot))
  
  return (log_like)
  
}

log_lh_2 <- function(data, par_2) {
  
  m_p1 <- par_2[1]  
  m_p2 <- par_2[2]
  lamb_p <- par_2[3]
  c <- par_2[4]
  lamb_a <- par_2[5]
  lamb_b <- par_2[6]
  s1 <- par_2[7]
  s2 <- par_2[8]
  alpha <- par_2[9]
  beta <- par_2[10]
  gama <- par_2[11]
 
  l <- data[ , "decaylength" ]
  m <- data[ , "mass" ]
  
  #The particles' p1, p2 masses follow a gaussian distribution
  
  pdf_mp1 <- (1/( s1 * sqrt(2*pi)))*(exp(-(m-m_p1)*(m-m_p1)/(2*s1*s1)))
  pdf_mp2 <- (1/( s2 * sqrt(2*pi)))*(exp(-(m-m_p2)*(m-m_p2)/(2*s2*s2)))
  
  #The particles have a only exponential distribution of decay length 
  
  pdf_lamb_p <- (1/lamb_p)*(exp((-1)*l/lamb_p))
  
  #The length of the background noise follows a exponential decay
  
  pdf_lamb_a <- (1/lamb_a)*(exp((-1)*l/lamb_a))
  pdf_lamb_b <- (1/lamb_b)*(exp((-1)*l/lamb_b))                      
  
  #The background noise mass follows the next distributions
  
  N <- (max(m)-min(m)) - 0.5*c*(max(m)*max(m)-min(m)*min(m))
  
  pdf_noise_a <- 1/(max(m)-min(m))
  pdf_noise_b <- (1- c*m)/N
  
  # We compute the total pdf
  
  pdf_tot <- alpha*pdf_mp1*pdf_lamb_p + beta*pdf_mp2*pdf_lamb_p + gama*pdf_lamb_a*pdf_noise_a + (1-alpha-beta-gama)*pdf_lamb_b*pdf_noise_b 
  log_like <- sum(log(pdf_tot))
  
  return (log_like) 
  
}

dv <- function (fn, data, pm, i){
  h <- 1e-4
  fn_pm <- fn(data, pm)
  pm[i] <- pm[i]+h
  fn_h <- fn(data, pm)
  der <- ( fn_h - fn_pm ) / h
  return(der)
}

dv2 <- function (fn, data, pm, i, j){
  h <- 1e-4
  fn_pm <- fn(data, pm)
  pm[i] <- pm[i]+h
  fn_i <- fn(data, pm)
  pm[j] <- pm[j]+h
  fn_ij <- fn(data, pm)
  pm[i] <- pm[i]-h
  fn_j <- fn(data, pm)
  der2 <- (fn_ij - fn_i - fn_j + fn_pm) / (h*h)
  return(der2)
}


compute_errors <- function(data, x_0, i , f, s){
  
  diff.like <- c()
  diff.like2 <- c()
  int <- x_0[i]/15
  x_0i_test <- seq(from = x_0[i] - int, to = x_0[i] + int, by = int/100)
  
  for (k in x_0i_test) {
    
    par_test <- c(x_0)
    par_test[i] = k
    deltal <- f(data, par_test)
    l <- log_lh(df, x_0)
    
    diff.like <- c(diff.like,abs(deltal - l + 0.5))
    diff.like2 <- c(diff.like2, deltal - l)
  }
  
  plot(x_0i_test, diff.like, xlab = s[i], ylab = "Difference", title = "")
  print (s[i])
  
  central_point <- which.max(diff.like2)
  minimum_left <- which.min(diff.like[1: central_point])
  minimum_right <- central_point + which.min(diff.like[central_point:length(diff.like)])
  sigma_l <- x_0i_test[central_point] - x_0i_test[minimum_left]
  sigma_r <- x_0i_test[minimum_right] - x_0i_test[central_point]
  
  print(sprintf('Central point: %f', x_0i_test[central_point]))
  print(sprintf ('Sigma left: %f ', sigma_l))
  print(sprintf ('Sigma right: %f ', sigma_r))
  
  x_results <- c(x_0i_test[central_point], sigma_l, sigma_r)
  return(x_results)
}
