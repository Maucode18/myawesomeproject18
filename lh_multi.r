#! usr/bin/env Rscript

#Método Newton, derivada de likelihood

library(MASS)

setwd("~/CCII/myawesomeproject18/")
df <- read.table("data/random_exp.csv", header = T)

lh <- function (data_array, par) {
  
  alpha <- par[1]
  tau_s <- par [2]
  tau_l <- par [3]
  
  pdf_s <- exp(-data_array/tau_l)/tau_l
  pdf_l <- exp(-data_array/tau_s)/tau_s
  pdf_total <- alpha * pdf_s + (1-alpha) * pdf_l
  
  total <- sum(log(pdf_total))
  
  return (total)
  
}

D <- function (f, data_array, par, i) { #partial derivative over i-th variable
  
  dx <- 1e-4
  f_x <- f(data_array, par)
  par[i]  <- par[i] + dx
  f_delta <- f(data_array, par) #Incremento asignado en la línea anterior
  der_f <- (f_delta - f_x)/ (dx)
  
  return (der_f)
  
}

D_2 <- function (f, data_array, par, i, j) {
  
  dx <- 1e-4
  f_x <- f(data_array, par)
  par[i] <- par[i] + dx
  del_fi <- f(data_array, par)
  par[j] <- par[j] + dx
  del_fij <- f(data_array, par)
  par[i] <- par[i] - dx
  del_fj <- f(data_array, par)
  seg <- (del_fij - del_fj -del_fi + f_x)/(dx*dx)
  
  return(seg)
}

x0 <- c(0.6, 90, 500)
test <- D(lh, df$decaytime, x0, 2)
test2 <- D_2(lh, df$decaytime, x0, 2, 2)

print(test)
print(test2)

def_f <- c(1:3)
jac_f <- matrix(1:9, nrow = 3)
eps <- 1e-4
difference <- 1000
counter <- 0

while(abs(difference)>eps) {
  
  for(i in 1:3){
    def_f[i] <- D(lh, df$decaytime, x0, i) #vector de primeras derivadas
    for(j in 1:3){
      jac_f[i,j] <- D_2(lh, df$decaytime, x0, i, j) #matriz de segundas derivadas
    }
  }

  print(jac_f)  
inv_jac <- ginv(jac_f)
Jf <- inv_jac %*% def_f
xn <- x0 - Jf

a <- (xn-x0)*(xn-x0)
difference <- sum(a)
x0 <- xn
counter <- counter + 1
}

xn1_test <- seq(from = xn[1]-0.02, to = xn[1]+0.02, by = 0.001)
diff.like <- c()
diff.like2 <- c()

for (k in xn1_test) {
  
  par_test <- c(xn)
  par_test[1] = k
  deltal <- lh(df$decaytime, par_test)
  l <- lh(df$decaytime, xn)
  
  diff.like <- c(diff.like,abs(deltal - l + 0.5))
  diff.like2 <- c(diff.like2, deltal - l)
}

plot(xn1_test, diff.like)
central_point <- which.max(diff.like2)
minimum_left <- which.min(diff.like[1: central_point])
minimum_right <- central_point + which.min(diff.like[central_point:length(diff.like)])
sigma_l <- xn1_test[central_point] - xn1_test[minimum_left]
sigma_r <- xn1_test[minimum_right] - xn1_test[central_point]
print(xn)
print (xn1_test[minimum_left])
print (xn1_test[minimum_right])

print(sprintf('Central point: %f', xn1_test[central_point]))
print(sprintf ('Sigma left: %f ', sigma_l))
print(sprintf ('Sigma right: %f ', sigma_r))

breik <- 50
step <- (max(df$decaytime)- min(df$decaytime))/breik

breiks <- seq(min(df$decaytime), max(df$decaytime), step)
hdatos <- hist(df$decaytime, breaks = breiks)
ni <- hdatos$counts

alpha <- xn[1]
tau_s <- xn[2]
tau_l <- xn[3]

model_large <- exp(-hdatos$mids/tau_l)/tau_l
model_short <- exp(-hdatos$mids/tau_s)/tau_s
model_total <- alpha * model_short + (1-alpha) * model_large

gnu_i = model_total * step * length(df$decaytime)

print(gnu_i)
print(ni)