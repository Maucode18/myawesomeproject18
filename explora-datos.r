#!/usr/bin/env Rscript

path <- getwd()
setwd("~/CCII/myawesomeproject18/")
print(path)
#leer un archivo
df <- read.table("data.csv", header= T)
summary(df)
h <- hist(df$decaytime)
y <- log(h$density)
x <- h$mids

n <- length(x)
sum_x <- sum(x)
sum_y <- sum(y)
sum_xy <- sum(x*y)
sum_x2 <- sum(x*x)

m <- (n*sum_xy - sum_y*sum_x)/(n*sum_x2 - (sum_x)^2)
b <- (sum_y - m*sum_x)/n

#etiquetas
title_x <- "Tiempo decaimiento (ps)"
title_y <- "Logaritmo de densidad de frecuencia (log(1/ps))"

png(filename = "ejemplo.png")

plot(x,y, xlab = title_x, ylab = title_y)
leg <- sprintf("y = %f * x + %f", m , b)
legend(x = 1150, y = -8, legend = leg, cex = 0.7, box.lwd = 0) 
abline (a = b, b = m) #(a = intercepto, b = pendiente)




